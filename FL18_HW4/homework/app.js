function reverseNumber(num) {
    const number_toString = num.toString();
    let reversedNumber = ""

    if(num < 0){
        reversedNumber = "-"
        for(let i = number_toString.length-1; i>0; i--){
            reversedNumber = reversedNumber + number_toString[i]
        }
        return reversedNumber
    }
    else{
        for(let i = number_toString.length-1; i>0; i--){
            reversedNumber = reversedNumber + number_toString[i]
        }
        return reversedNumber
    }
}
console.log(reverseNumber(12345))
console.log(reverseNumber(-56789))

function forEach(arr, func) {
    for(let i = 0; i < arr.length; i++){
        func(arr[i])
    }
}
forEach([2,5,8], function(el) { console.log(el) }) 

function map(arr, func) {
    const result = []
    for(let i =0; i< arr.length; i++){
        result.push(func(arr[i]))
    }
    return result;
}
console.log(map([2, 5, 8], function(el) { return el + 3; }))
console.log(map([1, 2, 3, 4, 5], function (el) { return el * 2; }))

function filter(arr, func) {
    const result = []
    forEach(arr, (el) => { if(func(el)) result.push(el) })
    return result
}
console.log(filter([2, 5, 1, 3, 8, 6], function(el) { return el > 3 }))


let data = [{
    "_id": "5b5e3168c6bf40f2c1235cd6",
    "index": 0,
    "age": 39,
    "eyeColor": "green",
    "name": "Stein",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5b5e3168e328c0d72e4f27d8",
    "index": 1,
    "age": 38,
    "eyeColor": "blue",
    "name": "Cortez",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5b5e3168cc79132b631c666a",
    "index": 2,
    "age": 2,
    "eyeColor": "blue",
    "name": "Suzette",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5b5e31682093adcc6cd0dde5",
    "index": 3,
    "age": 17,
    "eyeColor": "green",
    "name": "Weiss",
    "favoriteFruit": "banana"
  }
];

function getAdultAppleLovers(data) {
    return map(filter(data, user => user.age >= 18 && user.favoriteFruit == 'apple'), user => user.name);
}
console.log(getAdultAppleLovers(data))

function getKeys(obj) {
    let newArray = []
    for(let item in obj )
    {
        newArray.push(item)
    }
    return newArray
}
console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}))

function getValues(obj) {
    let newArray = []
    for(let number in obj)
    {
        let value = obj[number]
        newArray.push(value)
    }
    return (newArray)
}
console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}))

function showFormattedDate(dateObj) {
    new_date = dateObj
    return 'It is ' + new_date
}
console.log(showFormattedDate(new Date('2018-08-27T01:10:00')))
