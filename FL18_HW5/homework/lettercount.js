function letterCount(word, letter){
    let count = 0;
    
    if(word.includes(letter)){ 
        for(let i =0; i < word.length; i++)
            count++;
    }

    return count
}
console.log(letterCount("Mary", "r"))
console.log(letterCount("Barny", "y"))
console.log(letterCount("", "z"))